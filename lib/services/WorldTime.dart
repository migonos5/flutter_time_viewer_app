import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:intl/intl.dart';

class WorldTime {
  static Future<Map> getTime(
      {required String continent, required String city}) async {
    var response = await http.get(
        Uri.parse("http://worldtimeapi.org/api/timezone/$continent/$city"));
    Map data = jsonDecode(response.body);

    String datetime = data['datetime'];
    List<String> offset = data['utc_offset'].split(":");

    DateTime now = DateTime.parse(datetime);
    now = now.add(Duration(hours: int.parse(offset[0])));

    print(int.parse(DateFormat()
        .add_jm()
        .format(now.add(Duration(hours: int.parse(offset[0]))))
        .split(":")[0]));

    return {
      "hour": DateFormat().add_jm().format(now),
      "isDayTime": now.hour >= 5 && now.hour < 18 ? true : false
    };
  }
}
