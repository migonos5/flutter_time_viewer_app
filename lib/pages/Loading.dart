import 'package:flutter/material.dart';
import 'package:times/services/WorldTime.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loading extends StatefulWidget {
  const Loading({Key? key}) : super(key: key);

  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {
  String date = "Loading";

  @override
  void initState() {
    getTime();
    super.initState();
  }

  void getTime() async {
    try {
      Map newDate =
          await WorldTime.getTime(continent: "America", city: "Guayaquil");
      Navigator.pushReplacementNamed(context, "/home", arguments: {
        'date': newDate["hour"],
        "isDayTime": newDate["isDayTime"]
      });
    } catch (e) {
      setState(() {
        date = "An error has occured";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      body: Center(
        child: SpinKitWave(
          color: Colors.teal,
          size: 80,
        ),
      ),
    );
  }
}
