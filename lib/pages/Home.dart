import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Map data = {};
  String selectedCity = "Guayaquil";

  @override
  Widget build(BuildContext context) {
    data =
        data.isEmpty ? ModalRoute.of(context)!.settings.arguments as Map : data;

    String backgroundImageName = data["isDayTime"] ? "day.png" : "night.png";

    return Scaffold(
      backgroundColor: data["isDayTime"] ? Colors.blue : Colors.indigo[700],
      body: SafeArea(
          child: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/$backgroundImageName"),
                fit: BoxFit.cover)),
        child: Center(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 120, 0, 20),
            child: Column(
              children: <Widget>[
                TextButton.icon(
                    onPressed: () async {
                      dynamic result = await Navigator.pushNamed(
                          context, "/chooseLocation",
                          arguments: {"isDayTime": data["isDayTime"]});
                      print(result["isDayTime"]);
                      setState(() {
                        data = {
                          "date": result["date"],
                          "isDayTime": result["isDayTime"],
                        };
                        selectedCity = result["selectedCity"];
                      });
                    },
                    icon: Icon(
                      Icons.edit_location,
                      color: data["isDayTime"]
                          ? Colors.grey.shade800
                          : Colors.teal.shade800,
                      size: 40,
                    ),
                    label: Text(
                      "Change Location",
                      style: TextStyle(
                          color: data["isDayTime"]
                              ? Colors.grey.shade800
                              : Colors.teal.shade800,
                          fontSize: 20),
                    )),
                SizedBox(
                  height: 40,
                ),
                Text(
                  selectedCity,
                  style: TextStyle(
                      fontSize: 20,
                      color:
                          data["isDayTime"] ? Colors.grey : Colors.tealAccent),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  data["date"],
                  style: TextStyle(
                      fontSize: 60,
                      color: data["isDayTime"]
                          ? Colors.grey.shade700
                          : Colors.tealAccent[700]),
                )
              ],
            ),
          ),
        ),
      )),
    );
  }
}
