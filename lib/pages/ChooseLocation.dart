import 'package:flutter/material.dart';
import 'package:times/services/WorldTime.dart';

class ChooseLocation extends StatefulWidget {
  const ChooseLocation({Key? key}) : super(key: key);

  @override
  _ChooseLocationState createState() => _ChooseLocationState();
}

class _ChooseLocationState extends State<ChooseLocation> {
  List<String> continents = [
    "Africa",
    "America",
    // "Antarctica",
    // "Asia",
    // "Atlantic",
    // "Australia",
    // "Europe",
    // "Indian",
    // "Pacific"
  ];
  List<String> africa = [
    "Abidjan",
    "Accra",
    "Algiers",
    "Bissau",
    "Cairo",
    "Casablanca",
    "Ceuta",
    "El_Aaiun",
    "Johannesburg",
    "Juba",
    "Khartoum",
    "Lagos",
    "Maputo",
    "Monrovia",
    "Nairobi",
    "Ndjamena",
    "Sao_Tome",
    "Tripoli",
    "Tunis",
    "Windhoek"
  ];
  List<String> america = [
    "Adak",
    "Anchorage",
    "Araguaina",
    "Argentina/Buenos_Aires",
    "Argentina/Catamarca",
    "Argentina/Cordoba",
    "Argentina/Jujuy",
    "Argentina/La_Rioja",
    "Argentina/Mendoza",
    "Argentina/Rio_Gallegos",
    "Argentina/Salta",
    "Argentina/San_Juan",
    "Argentina/San_Luis",
    "Argentina/Tucuman",
    "Argentina/Ushuaia",
    "Asuncion",
    "Atikokan",
    "Bahia",
    "Bahia_Banderas",
    "Barbados",
    "Belem",
    "Belize",
    "Blanc-Sablon",
    "Boa_Vista",
    "Bogota",
    "Boise",
    "Cambridge_Bay",
    "Campo_Grande",
    "Cancun",
    "Caracas",
    "Cayenne",
    "Chicago",
    "Chihuahua",
    "Costa_Rica",
    "Creston",
    "Cuiaba",
    "Curacao",
    "Danmarkshavn",
    "Dawson",
    "Dawson_Creek",
    "Denver",
    "Detroit",
    "Edmonton",
    "Eirunepe",
    "El_Salvador",
    "Fort_Nelson",
    "Fortaleza",
    "Glace_Bay",
    "Goose_Bay",
    "Grand_Turk",
    "Guatemala",
    "Guayaquil",
    "Guyana",
    "Halifax",
    "Havana",
    "Hermosillo",
    "Indiana/Indianapolis",
    "Indiana/Knox",
    "Indiana/Marengo",
    "Indiana/Petersburg",
    "Indiana/Tell_City",
    "Indiana/Vevay",
    "Indiana/Vincennes",
    "Indiana/Winamac",
    "Inuvik",
    "Iqaluit",
    "Jamaica",
    "Juneau",
    "Kentucky/Louisville",
    "Kentucky/Monticello",
    "La_Paz",
    "Lima",
    "Los_Angeles",
    "Maceio",
    "Managua",
    "Manaus",
    "Martinique",
    "Matamoros",
    "Mazatlan",
    "Menominee",
    "Merida",
    "Metlakatla",
    "Mexico_City",
    "Miquelon",
    "Moncton",
    "Monterrey",
    "Montevideo",
    "Nassau",
    "New_York",
    "Nipigon",
    "Nome",
    "Noronha",
    "North_Dakota/Beulah",
    "North_Dakota/Center",
    "North_Dakota/New_Salem",
    "Nuuk",
    "Ojinaga",
    "Panama",
    "Pangnirtung",
    "Paramaribo",
    "Phoenix",
    "Port-au-Prince",
    "Port_of_Spain",
    "Porto_Velho",
    "Puerto_Rico",
    "Punta_Arenas",
    "Rainy_River",
    "Rankin_Inlet",
    "Recife",
    "Regina",
    "Resolute",
    "Rio_Branco",
    "Santarem",
    "Santiago",
    "Santo_Domingo",
    "Sao_Paulo",
    "Scoresbysund",
    "Sitka",
    "St_Johns",
    "Swift_Current",
    "Tegucigalpa",
    "Thule",
    "Thunder_Bay",
    "Tijuana",
    "Toronto",
    "Vancouver",
    "Whitehorse",
    "Winnipeg",
    "Yakutat",
    "Yellowknife"
  ];

  Map data = {};
  String selectedContinent = "America";
  String selectedCity = "Guayaquil";

  List<DropdownMenuItem<String>> selectedCities = [];

  List<DropdownMenuItem<String>> getCities(List<String> list) {
    return list.map<DropdownMenuItem<String>>((String value) {
      return DropdownMenuItem<String>(
        value: value,
        child: Text(value),
      );
    }).toList();
  }

  List<String> getSelectedCities(String cityName) {
    switch (cityName) {
      case "America":
        print("About to return america");
        selectedCity = "Guayaquil";
        return america;
      case "Africa":
        print("about to return africa");
        selectedCity = "Cairo";
        return africa;
      default:
        return america;
    }
  }

  @override
  Widget build(BuildContext context) {
    data = ModalRoute.of(context)!.settings.arguments as Map;

    return Scaffold(
        backgroundColor: data["isDayTime"] ? Colors.blue : Colors.indigo[700],
        appBar: AppBar(
          backgroundColor: data["isDayTime"] ? Colors.blue : Colors.indigo[700],
          title: Text("Choose a location"),
          centerTitle: true,
          elevation: 0,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () async {
              try {
                Map newDate = await WorldTime.getTime(
                    continent: selectedContinent, city: selectedCity);
                Navigator.pop(context, {
                  "date": newDate["hour"],
                  "isDayTime": newDate["isDayTime"],
                  "selectedCity": selectedCity
                });
              } catch (e) {
                Navigator.pop(context, {
                  "date": "Ha ocurrido un error inesperado",
                  "isDayTime": true,
                  "selectedCity": ""
                });
              }
            },
          ),
        ),
        body: Center(
          child: Padding(
            padding: EdgeInsets.fromLTRB(0, 120, 0, 20),
            child: Column(
              children: [
                Text(
                  "Continente",
                  style: TextStyle(
                      fontSize: 20,
                      color:
                          data["isDayTime"] ? Colors.grey : Colors.tealAccent),
                ),
                SizedBox(
                  height: 10,
                ),
                DropdownButton<String>(
                  value: selectedContinent,
                  icon: const Icon(Icons.arrow_downward),
                  iconSize: 24,
                  elevation: 16,
                  style: TextStyle(
                      color: data["isDayTime"]
                          ? Colors.grey.shade700
                          : Colors.tealAccent[700]),
                  onChanged: (String? newValue) {
                    setState(() {
                      selectedContinent = newValue!;
                      selectedCities = getCities(getSelectedCities(newValue));
                    });
                  },
                  dropdownColor:
                      data["isDayTime"] ? Colors.blue : Colors.indigo[700],
                  items:
                      continents.map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
                SizedBox(
                  height: 40,
                ),
                Text(
                  "Ciudad",
                  style: TextStyle(
                      fontSize: 20,
                      color:
                          data["isDayTime"] ? Colors.grey : Colors.tealAccent),
                ),
                SizedBox(
                  height: 10,
                ),
                DropdownButton<String>(
                    value: selectedCity,
                    icon: const Icon(Icons.arrow_downward),
                    iconSize: 24,
                    elevation: 16,
                    style: TextStyle(
                        color: data["isDayTime"]
                            ? Colors.grey.shade700
                            : Colors.tealAccent[700]),
                    onChanged: (String? newValue) {
                      setState(() {
                        selectedCity = newValue!;
                      });
                    },
                    dropdownColor:
                        data["isDayTime"] ? Colors.blue : Colors.indigo[700],
                    items: selectedCities.isEmpty
                        ? getCities(america)
                        : selectedCities),
              ],
            ),
          ),
        ));
  }
}
